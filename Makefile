SOURCES:=$(shell find src -name *.vala)
PACKAGES:=gtk+-3.0

REQUIREMENTS:=gtk3-devel

BUILDDIR:=build

VALAC:=valac

main: $(SOURCES)
	$(VALAC) -d $(BUILDDIR) $(shell echo $(PACKAGES) | sed -r 's/([^ ]+)/--pkg \1/g') $(SOURCES)
	
install-requirements:
	dnf install -y $(REQUIREMENTS)
