# GTK3 App Base

Basic application frame for GUI applications with GTK3 and Vala.

## Structure

- `src`: Contains all `.vala` source files.
- `build`: Generated on build. Contains the built executable and other relevant
  build files.

This project uses *make* for building. Targets:

- *targetless invocation*: Builds the executable.
- `install-requirements`: Installs relevant packages needed for built from the
  system package manager.

You can freely augment necessary Vala packages, sources and others inside the
`Makefile`.
